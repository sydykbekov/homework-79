CREATE SCHEMA `office` ;
USE `office`;

CREATE TABLE `office`.`Категория предмета` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Название` VARCHAR(255) NOT NULL,
    `Описание` VARCHAR(255) NOT NULL
);

INSERT INTO `Категория предмета` (`Название`, `Описание`) VALUES ('Notebook', 'Technology');

CREATE TABLE `office`.`Местоположение предмета` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Название` VARCHAR(255) NOT NULL,
    `Описание` VARCHAR(255) NOT NULL
);

INSERT INTO `Местоположение предмета` (`Название`, `Описание`) VALUES ('Store', 'two-chamber');

CREATE TABLE `office`.`Предмет учета` (
  `Идентификатор` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `Название` VARCHAR(255) NOT NULL,
  `Категория_id` INT NOT NULL,
  `Местоположение_id` INT NOT NULL,
  `Описание` VARCHAR(255) NOT NULL,
  `Дата постановки на учет` VARCHAR(255) NOT NULL,
  INDEX `FK_Категория_idx` (`Категория_id`),
		FOREIGN KEY (`Категория_id`)
        REFERENCES `Категория предмета` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
  INDEX `FK_Местоположение_idx` (`Местоположение_id`),
		FOREIGN KEY (`Местоположение_id`)
        REFERENCES `Местоположение предмета` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE	
);

INSERT INTO `Предмет учета` (`Название`, `Категория_id`, `Местоположение_id`, `Описание`, `Дата постановки на учет`) 
VALUES ('Notebook', 1, 1, 'New notebook', '19.04.2018');

SELECT * FROM `Предмет учета`;